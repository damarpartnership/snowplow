Readme

===============

pg_ctl -D /usr/local/var/postgres start





===============


CREATE USER damar_analytics;
CREATE DATABASE damar_analytics WITH OWNER damar_analytics ENCODING 'UTF8';
ALTER USER "damar_analytics" WITH PASSWORD 'damar_analytics';





psql -h localhost -U damar_analytics -d damar_analytics -p 5432 -f atomic-def.sql
psql -h localhost -U damar_analytics -d damar_analytics -p 5432 -f recipes-basic.sql
psql -h localhost -U damar_analytics -d damar_analytics -p 5432 -f recipes-catalog.sql
psql -h localhost -U damar_analytics -d damar_analytics -p 5432 -f recipes-customers.sql


CREATE USER power_user SUPERUSER;
ALTER USER power_user WITH PASSWORD 'damar_analytics';
CREATE USER damar_analytics NOSUPERUSER;
ALTER USER damar_analytics WITH PASSWORD 'damar_analytics';
CREATE USER storageloader PASSWORD 'damar_analytics';
\q

psql -h localhost -U power_user -d damar_analytics -p 5432
GRANT USAGE ON SCHEMA atomic TO storageloader;
GRANT INSERT ON TABLE   "atomic"."events" TO storageloader;

GRANT USAGE ON SCHEMA   atomic   TO damar_analytics;
GRANT USAGE ON SCHEMA   recipes_basic    TO damar_analytics;
GRANT USAGE ON SCHEMA   recipes_catalog  TO damar_analytics;
GRANT USAGE ON SCHEMA   recipes_customer     TO damar_analytics;

GRANT SELECT ON     atomic  .   events   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   uniques_and_visits_by_day    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   pageviews_by_day     TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   events_by_day    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   pages_per_visit  TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   bounce_rate_by_day   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   fraction_new_visits_by_day   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   avg_visit_duration_by_day    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   visitors_by_language     TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   visits_by_country    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   new_vs_returning     TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   behavior_frequency   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   behavior_recency     TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   engagement_visit_duration    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   engagement_pageviews_per_visit   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   technology_browser   TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   technology_os    TO damar_analytics;
GRANT SELECT ON     recipes_basic   .   technology_mobile    TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   uniques_and_pvs_by_page_by_month     TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   uniques_and_pvs_by_page_by_week  TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   add_to_baskets_by_page_by_month  TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   add_to_baskets_by_page_by_week   TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   purchases_by_product_by_month    TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   purchases_by_product_by_week     TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   all_product_metrics_by_month     TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   time_and_fraction_read_per_page_per_user     TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   pings_per_page_per_month     TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   avg_pings_per_unique_per_page_per_month  TO damar_analytics;
GRANT SELECT ON     recipes_catalog .   traffic_driven_to_site_per_page_per_month    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_domain_to_network     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_domain_to_user    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_domain_to_ipaddress   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_domain_to_fingerprint     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_network_to_user   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_network_to_ipaddress  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_network_to_fingerprint    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_user_to_ipaddress     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_user_to_fingerprint   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   id_map_ipaddress_to_fingerprint  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   clv_total_transaction_value_by_user_by_month     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   clv_total_transaction_value_by_user_by_week  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   engagement_users_by_days_p_month_on_site     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   engagement_users_by_days_p_week_on_site  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   engagement_users_by_visits_per_month     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   engagement_users_by_visits_per_week  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_month_first_touch_website  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_week_first_touch_website   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_month_signed_up    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_week_signed_up     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_month_first_transact   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_week_first_transact    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_paid_channel_acquired_by_month     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_paid_channel_acquired_by_week  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_refr_channel_acquired_by_month     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_dfn_by_refr_channel_acquired_by_week  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   retention_by_user_by_month   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   retention_by_user_by_week    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_month_first_touch    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_week_first_touch     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_month_signed_up  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_week_signed_up   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_month_first_transact     TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_week_first_transact  TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_month_by_paid_channel_acquired   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_week_by_paid_channel_acquired    TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_month_by_refr_acquired   TO damar_analytics;
GRANT SELECT ON     recipes_customer    .   cohort_retention_by_week_by_refr_acquired    TO damar_analytics;
\q




GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "atomic"."events" TO damar_analytics;