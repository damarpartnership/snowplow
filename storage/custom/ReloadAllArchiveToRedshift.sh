#!/bin/bash

# setup AWS CLI first
#  http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-set-up.html
export AWS_DEFAULT_REGION=eu-west-1
# Set AWS access key and secret  unless set in enviroment 
# export AWS_ACCESS_KEY_ID= 
# export AWS_SECRET_ACCESS_KEY=


# 0) MAKE A BACKUP OF EVERYTHING!!!
# aws s3 cp s3://damar-analytics-enrich/ s3://damar-analytics-bakup/ --recursive 
# 1) Make a backup (s3 or locally) 
# aws s3 cp s3://damar-analytics-enrich/ /Users/fredrikwahlqvist/Desktop/s3backup/ --recursive
# 2) Copy all archived processed logfiles 
# aws s3 cp s3://damar-analytics-enrich/archive/ /Users/fredrikwahlqvist/Desktop/s3backup/ --recursive
# 3) Flatten folder structure
# find s3backup/ -mindepth 2 -type f -exec mv -i '{}' s3backup/ ';'
# 4) Upload files to a manual reload bucket
# aws s3 cp /Users/fredrikwahlqvist/Desktop/s3backup/ s3://damar-analytics-manual-reload/
# 5) Empty all previous processed folders 
# aws s3 rm --recursive s3://damar-analytics-enrich/archive/
# aws s3 rm --recursive s3://damar-analytics-enrich/enriched/archive/
# aws s3 rm --recursive s3://damar-analytics-enrich/enriched/bad/
# aws s3 rm --recursive s3://damar-analytics-enrich/enriched/errors/
# aws s3 rm --recursive s3://damar-analytics-enrich/enriched/good/
# aws s3 rm --recursive s3://damar-analytics-enrich/shredded/archive/
# aws s3 rm --recursive s3://damar-analytics-enrich/shredded/bad/
# aws s3 rm --recursive s3://damar-analytics-enrich/shredded/errors/
# aws s3 rm --recursive s3://damar-analytics-enrich/shredded/good/
# 6) Manully rerun the full process with the updated log file in the yml 
#
#
#
# s3 ls command
#  http://docs.aws.amazon.com/cli/latest/reference/s3/ls.html

# space-separated string (contains dates etc.)
#flist=$(aws s3 ls s3://damar-analytics-enrich/shredded/archive/)
#  http://stackoverflow.com/questions/15224535/bash-put-list-files-into-a-variable-and-but-size-of-array-is-1 
#echo ${flist[@]}
#http://docs.aws.amazon.com/cli/latest/reference/s3/cp.html 
#aws s3 cp s3://damar-analytics-enrich/archive/ /Users/fredrikwahlqvist/Desktop/s3backup/ --recursive
#aws s3 cp /Users/fredrikwahlqvist/Desktop/s3backup/ s3://damar-analytics-manual-reload/ --recursive
#aws s3 cp s3://damar-analytics-enrich/ /Users/fredrikwahlqvist/Desktop/s3backup/ --recursive
#find s3backup/ -mindepth 2 -type f -exec mv -i '{}' s3backup/ ';'
#
#
#
#