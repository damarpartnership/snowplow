{
  "$schema" : "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "self" : {
    "vendor" : "com.clearbit.enrichment",
    "name" : "company",
    "version" : "1-0-0",
    "format" : "jsonschema"
  },
  "type" : "object",
  "properties" : {
    "name" : {
      "type" : ["string", "null"]
    },
    "foundedYear" : {
     "type" : ["integer", "null"],
      "maximum" : 2147483647,
      "minimum" : 0
    },
    "crunchbase" : {
      "type" : "object",
      "properties" : {
        "handle" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "location" : {
      "type" : ["string", "null"]
    },
    "url" : {
      "type" : ["string", "null"],
      "format" : "uri"
    },
    "description" : {
      "type" : ["string", "null"]
    },
    "tags" : {
      "type" : "array",
      "items" : {
        "type" : ["string", "null"]
      }
    },
    "linkedin" : {
      "type" : "object",
      "properties" : {
        "handle" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "geo" : {
      "type" : "object",
      "properties" : {
        "city" : {
          "type" : ["string", "null"]
        },
        "stateCode" : {
          "type" : ["string", "null"]
        },
        "lng" : {
          "type" : ["number", "null"]
        },
        "state" : {
          "type" : ["string", "null"]
        },
        "country" : {
          "type" : ["string", "null"]
        },
        "streetName" : {
          "type" : ["string", "null"]
        },
        "postalCode" : {
          "type" : ["string", "null"]
        },
        "subPremise" : {
          "type" : "null"
        },
        "countryCode" : {
          "type" : ["string", "null"]
        },
        "streetNumber" : {
          "type" : ["string", "null"]
        },
        "lat" : {
          "type" : ["number", "null"],
          "minimum" : 0
        }
      },
      "additionalProperties" : false
    },
    "domain" : {
      "type" : ["string", "null"]
    },
    "site" : {
      "type" : "object",
      "properties" : {
        "metaDescription" : {
          "type" : ["string", "null"]
        },
        "url" : {
          "type" : ["string", "null"],
          "format" : "uri"
        },
        "emailAddresses" : {
          "type" : "array",
          "items" : {
            "type" : ["string", "null"]
          }
        },
        "phoneNumbers" : {
          "type" : "array",
          "items" : { }
        },
        "h1" : {
          "type" : ["string", "null"]
        },
        "title" : {
          "type" : ["string", "null"]
        },
        "metaAuthor" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "domainAliases" : {
      "type" : "array",
      "items" : {
        "type" : ["string", "null"]
      }
    },
    "emailProvider" : {
      "type" : "boolean"
    },
    "logo" : {
      "type" : ["string", "null"],
      "format" : "uri"
    },
    "id" : {
      "type" : ["string", "null"],
      "format" : "uuid"
    },
    "legalName" : {
      "type" : ["string", "null"]
    },
    "facebook" : {
      "type" : "object",
      "properties" : {
        "handle" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "tech" : {
      "type" : "array",
      "items" : {
        "type" : ["string", "null"]
      }
    },
    "category" : {
      "type" : "object",
      "properties" : {
        "sector" : {
          "type" : ["string", "null"]
        },
        "industryGroup" : {
          "type" : ["string", "null"]
        },
        "industry" : {
          "type" : ["string", "null"]
        },
        "subIndustry" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "metrics" : {
      "type" : "object",
      "properties" : {
        "employees" : {
         "type" : ["integer", "null"],
          "maximum" : 2147483647,
          "minimum" : 0
        },
        "marketCap" : {
          "type" : ["integer", "null"],
	  "maximum" : 9223372036854775807,
          "minimum" : 0
        },
        "employeesRange" : {
          "type" : ["string", "null"]
        },
        "raised" : {
         "type" : ["integer", "null"],
          "maximum" : 9223372036854775807,
          "minimum" : 0
        },
        "alexaUsRank" : {
         "type" : ["integer", "null"],
          "maximum" : 9223372036854775807,
          "minimum" : 0
        },
        "alexaGlobalRank" : {
         "type" : ["integer", "null"],
          "maximum" : 9223372036854775807,
          "minimum" : 0
        },
        "annualRevenue" : {
          "type" : "null"
        }
      },
      "additionalProperties" : false
    },
    "utcOffset" : {
     "type" : ["integer", "null"],
      "maximum" : 2147483647,
      "minimum" : -32768
    },
    "twitter" : {
      "type" : "object",
      "properties" : {
        "avatar" : {
          "type" : ["string", "null"],
          "format" : "uri"
        },
        "location" : {
          "type" : ["string", "null"]
        },
        "followers" : {
         "type" : ["integer", "null"],
          "maximum" : 2147483647,
          "minimum" : 0
        },
        "site" : {
          "type" : ["string", "null"],
          "format" : "uri"
        },
        "following" : {
         "type" : ["integer", "null"],
          "maximum" : 2147483647,
          "minimum" : 0
        },
        "bio" : {
          "type" : ["string", "null"]
        },
        "id" : {
          "type" : ["string", "null"]
        },
        "handle" : {
          "type" : ["string", "null"]
        }
      },
      "additionalProperties" : false
    },
    "type" : {
      "type" : ["string", "null"]
    },
    "ticker" : {
      "type" : "null"
    },
    "indexedAt" : {
      "type" : ["string", "null"],
      "format" : "date-time"
    },
    "timeZone" : {
      "type" : ["string", "null"]
    },
    "phone" : {
      "type" : "null"
    }
  },
  "additionalProperties" : false
}